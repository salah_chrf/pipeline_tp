<?php
use PHPUnit\Framework\TestCase;
use Tp\Test\Salarie;

class SalarieTest extends TestCase{
    /**
    * @dataProvider dataSetMatricule
    */
    public function testMatricule($mtrcl){
        $slr=new Salarie();
        $this->expectException(Exception::class);
        $slr->setMatricule($mtrcl);
    }

    public function dataSetMatricule(){
        return[
            [1245678990],
            [23]
        ];
    }

    /**
    * @dataProvider dataSetDateEmbauche
    */
    public function testDateEmbauche($date,$result){
        $slr=new Salarie(dateEmbauche:$date);
        $this->assertEquals($result,date_format($slr->getDateEmbauche(),'Y-m-d'));
    }

    public function dataSetDateEmbauche(){
        return[
            ["",date_format(new DateTime("now"),'Y-m-d')],
            ['10/12/2022',date_format(new DateTime('10/12/2022'),'Y-m-d')]
        ];
    }

    /**
    * @dataProvider dataSetExperience
    */
    public function testExperience($date,$result){
        $slr=new Salarie(dateEmbauche:$date);
        $this->assertEquals($result,$slr->experience());
    }

    public function dataSetExperience(){
        return[
            ["01/01/2010",12],
            ['12/30/2000',21]
        ];
    }
    
    /**
    * @dataProvider dataSetSalaireNet
    */
    public function testSalaireNet($salaire){
        $slr=new Salarie(salaire:$salaire);
        $this->assertEquals($salaire-($salaire*(Salarie::$tauxCS/100)),$slr->calculerSalaireNet());
    }

    public function dataSetSalaireNet(){
        return[
            [3000],
            [9000],
            [4000],
            [5000]
        ];
    }
    
    /**
    * @dataProvider dataSetPrime
    */
    public function testPrime($salaire,$date){
        $slr=new Salarie(salaire:$salaire,dateEmbauche:$date);
        $this->assertEquals((($salaire*0.8)+(100*$slr->experience())),$slr->primeAnnuelle());
    }

    public function dataSetPrime(){
        return[
            [3000,'10/12/2009'],
            [4000,'10/10/2003'],
            [5000,'10/11/2020'],
            [6000,'05/12/2010']
        ];
    }
}